resource "google_compute_firewall" "wu_egress" {
  count              = "${var.create_firewall == true ? 1 : 0}"
  name               = "${var.envname}-wu-egress-firewall"
  network            = "${var.gcp_network}"
  direction          = "EGRESS"
  destination_ranges = ["0.0.0.0/0"]

  allow {
    protocol = "tcp"
    ports    = ["443", "80"]
  }

  target_tags = ["wsus"]
}

resource "google_compute_firewall" "wsus_ingress" {
  count         = "${var.create_firewall == true ? 1 : 0}"
  name          = "${var.envname}-wsus-ingress-firewall"
  network       = "${var.gcp_network}"
  direction     = "INGRESS"
  source_ranges = ["0.0.0.0/0"]

  allow {
    protocol = "tcp"
    ports    = ["8530", "8531"]
  }

  target_tags = ["wsus"]
}
