locals {
  default_metadata = {
    envname                    = "${var.envname}"
    profile                    = "wsus"
    domain                     = "${var.ads_fq_domain_name}"
    windows-startup-script-ps1 = "${var.enable_gpo_creation == "1" ? "${data.template_file.wsus.rendered}${data.template_file.wsus_gpo.rendered}" : "${data.template_file.wsus.rendered}"}${var.startup_script}"
  }
}
