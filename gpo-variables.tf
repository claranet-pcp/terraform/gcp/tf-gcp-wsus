/*
    GPO Specific variables.
    Further information on these can be found here: https://msdn.microsoft.com/en-us/library/dd939844(v=ws.10).aspx

    Sane defaults have been applied however the two variables that _need_ to be set are
    
    WUServer        (http(s)://wsusserver.fq.dn:port)
    WUStatusServer  (http(s)://wsusserver.fq.dn:port)
    
    These values must be the same.
*/

## Registry keys for Windows Update: HKEY_LOCAL_MACHINE\Software\Policies\Microsoft\Windows\WindowsUpdate

/*  AcceptTrustedPublisherCerts
    DWORD
    Range = 1|0
    1 = Enabled. The WSUS server distributes available signed non-Microsoft updates.
    0 = Disabled. The WSUS server does not distribute available signed non-Microsoft updates. 
*/
variable "AcceptTrustedPublisherCerts" {
  type    = "string"
  default = 0
}

/*  DisableWindowsUpdateAccess
    DWORD
    Range = 1|0
    1 = Disables access to Windows Update.
    0 = Enables access to Windows Update.
*/
variable "DisableWindowsUpdateAccess" {
  type    = "string"
  default = 1
}

/*  ElevateNonAdmins
    DWORD
    Range = 1|0
    1 = All members of the Users security group can approve or disapprove updates.
    0 = Only members of the Administrators security group can approve or disapprove updates.
*/
variable "ElevateNonAdmins" {
  type    = "string"
  default = 0
}

/*  TargetGroup
    REG_SZ
    Name of the computer group to which the computer belongs. This policy is paired with TargetGroupEnabled.
*/
variable "TargetGroup" {
  type    = "string"
  default = "wsus-update-group"
}

/*  TargetGroupEnabled
    DWORD
    Range = 1|0
    1 = Use client-side targeting.
    0 = Do not use client-side targeting. This policy is paired with TargetGroup.
*/
variable "TargetGroupEnabled" {
  type    = "string"
  default = 0
}

/*  WUServer
    REG_SZ
    HTTP(S) URL of the WSUS server that is used by Automatic Updates and API callers (by default). This policy is paired with WUStatusServer, and both keys must be set to the same value to be valid.
    No default as this needs to be set to work.
*/
variable "WUServer" {
  type    = "string"
  default = ""
}

/*  WUStatusServer
    REG_SZ
    The HTTP(S) URL of the server to which reporting information is sent for client computers that use the WSUS server that is configured by the WUServer key. This policy is paired with WUServer, and both keys must be set to the same value to be valid.
    No default as this needs to be set to work.
*/
variable "WUStatusServer" {
  type    = "string"
  default = ""
}

## WSUS registry keys for Internet Explorer: HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer

/*  NoWindowsUpdate
    DWORD
    Range = 1|0
    1 = Enabled. Users cannot connect to the Windows Update website.
    0 = Disabled or not configured. Users can connect to the Windows Update website.
*/
variable "NoWindowsUpdate" {
  type    = "string"
  default = 1
}

## WSUS registry keys for Internet communication: HKEY_LOCAL_MACHINE\SYSTEM\Internet Communication Management\Internet Communication

/*  DisableWindowsUpdateAccess
    DWORD
    Range = 1|0
    1 = Enabled. All Windows Update features are removed. This includes blocking access to the Windows Update website at http://windowsupdate.microsoft.com, from the Windows Update hyperlink on the Start menu, and also on the Tools menu in Internet Explorer. Windows automatic updating is also disabled; you will neither be notified about nor will you receive critical updates from Windows Update. This setting also prevents Device Manager from automatically installing driver updates from the Windows Update website.
    0 = Disabled or not configured. Users will be able to access the Windows Update website and enable automatic updating to receive notifications and critical updates from Windows Update.
*/
variable "DisableWindowsUpdateAccess_IC" {
  type    = "string"
  default = 1
}

## WSUS registry key for Windows Update: HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Policies\WindowsUpdate

/*  DisableWindowsUpdateAccess
    DWORD
    Range = 1|0
    1 = Enabled. All Windows Update features are removed.
    0 = Disabled or not configured. All Windows Update features are available.
*/
variable "DisableWindowsUpdateAccess_WS" {
  type    = "string"
  default = 1
}

## Registry keys for Automatic Update configuration options: HKEY_LOCAL_MACHINE\Software\Policies\Microsoft\Windows\WindowsUpdate\AU

/*  AUOptions
    DWORD
    Range = 2|3|4|5
    2 = Notify before download.
    3 = Automatically download and notify of installation.
    4 = Automatically download and schedule installation. Only valid if values exist for ScheduledInstallDay and ScheduledInstallTime.
    5 = Automatic Updates is required and users can configure it.
*/
variable "AUOptions" {
  type    = "string"
  default = 4
}

/*  AutoInstallMinorUpdates
    DWORD
    Range = 1|0
    0 = Treat minor updates like other updates.
    1 = Silently install minor updates.
*/
variable "AutoInstallMinorUpdates" {
  type    = "string"
  default = 0
}

/*  DetectionFrequency
    DWORD
    Range = n, where n = time in hours (1–22).
    Time between detection cycles.
*/
variable "DetectionFrequency" {
  type    = "string"
  default = 12
}

/*  DetectionFrequencyEnabled
    DWORD
    Range = 0|1
    1 = Enable detection frequency.
    0 = Disable custom detection frequency (use default value of 22 hours).
*/
variable "DetectionFrequencyEnabled" {
  type    = "string"
  default = 1
}

/*  NoAutoRebootWithLoggedOnUsers
    DWORD
    Range = 0|1
    1 = Logged-on user can decide whether to restart the client computer.
    0 = Automatic Updates notifies the user that the computer will restart in 15 minutes.
*/
variable "NoAutoRebootWithLoggedOnUsers" {
  type    = "string"
  default = 1
}

/*  NoAutoUpdate
    DWORD
    Range = 0|1
    0 = Enable Automatic Updates.
    1 = Disable Automatic Updates.
*/
variable "NoAutoUpdate" {
  type    = "string"
  default = 1
}

/*  RebootRelaunchTimeout
    DWORD
    Range = n, where n = time in minutes (1–1,440).
    Time between prompts for a scheduled restart.
*/
variable "RebootRelaunchTimeout" {
  type    = "string"
  default = 240
}

/*  RebootRelaunchTimeoutEnabled
    DWORD
    Range = 0|1
    1 = Enable RebootRelaunchTimeout.
    0 = Disable custom RebootRelaunchTimeout(use default value of 10 minutes).
*/
variable "RebootRelaunchTimeoutEnabled" {
  type    = "string"
  default = 1
}

/*  RebootWarningTimeout
    DWORD
    Range = n, where n = time in minutes (1–30).
    Length, in minutes, of the restart warning countdown after updates have been installed that have a deadline or scheduled updates.
*/
variable "RebootWarningTimeout" {
  type    = "string"
  default = 30
}

/*  RebootWarningTimeoutEnabled
    DWORD
    Range = 0|1
    1 = Enable RebootWarningTimeout.
    0 = Disable custom RebootWarningTimeout (use default value of 5 minutes).
*/
variable "RebootWarningTimeoutEnabled" {
  type    = "string"
  default = 1
}

/*  RescheduleWaitTime
    DWORD
    Range = n, where n = time in minutes (1–60).
    Time in minutes that Automatic Updates waits at startup before it applies updates from a missed scheduled installation time.
    This policy applies only to scheduled installations, not to deadlines. Updates with deadlines that have expired should always be installed as soon as possible.
*/
variable "RescheduleWaitTime" {
  type    = "string"
  default = 60
}

/*  RescheduleWaitTimeEnabled
    DWORD
    Range = 0|1
    1 = Enable RescheduleWaitTime .
    0 = Disable RescheduleWaitTime (attempt the missed installation during the next scheduled installation time).
*/
variable "RescheduleWaitTimeEnabled" {
  type    = "string"
  default = 1
}

/*  ScheduledInstallDay
    DWORD
    Range = 0|1|2|3|4|5|6|7
    0 = Every day.
    1 through 7 = the days of the week from Sunday (1) to Saturday (7).
    (Only valid if AUOptions = 4.)
*/
variable "ScheduledInstallDay" {
  type    = "string"
  default = 1
}

/*  ScheduledInstallTime
    DWORD
    Range = n, where n = the time of day in 24-hour format (0–23).
*/
variable "ScheduledInstallTime" {
  type    = "string"
  default = 0
}

/*  UseWUServer
    DWORD
    Range = 0|1
    1 = The computer gets its updates from a WSUS server.
    0 = The computer gets its updates from Microsoft Update.
    The WUServer value is not respected unless this key is set.
*/
variable "UseWUServer" {
  type    = "string"
  default = 1
}
