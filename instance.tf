resource "google_compute_disk" "wsus_volume" {
  name = "${var.envname}-wsus-volume"
  type = "${var.wsus_drive_type}"
  zone = "${element(data.google_compute_zones.zones.names,0)}"
  size = "${var.wsus_drive_size}"
}

resource "google_compute_address" "wsus_ip" {
  name         = "${var.envname}-wsus-ip"
  address_type = "INTERNAL"
  subnetwork   = "${var.gcp_network}"
}

resource "google_compute_instance" "wsus" {
  name         = "${var.vm_name}"
  machine_type = "${var.gcp_machine_type}"
  zone         = "${element(data.google_compute_zones.zones.names,0)}"

  tags = [
    "${var.tags}",
  ]

  metadata = "${merge(local.default_metadata, var.additional_metadata)}"

  service_account {
    email  = "${var.gcp_service_account_email}"
    scopes = "${var.gcp_service_account_scopes}"
  }

  boot_disk {
    initialize_params {
      image = "${var.os_version}"
      type  = "${var.disk_type}"
    }
  }

  attached_disk {
    source = "${google_compute_disk.wsus_volume.self_link}"
  }

  network_interface {
    subnetwork = "${var.gcp_network}"
    network_ip = "${google_compute_address.wsus_ip.address}"
  }

  labels = "${var.labels}"

  deletion_protection = "${var.deletion_protection}"

  # Currently bugged. See: https://github.com/terraform-providers/terraform-provider-google/issues/1108
  # Caused by core bug: https://github.com/hashicorp/terraform/issues/17411
  # allow_stopping_for_update = "${var.allow_stopping_for_update}"
}
