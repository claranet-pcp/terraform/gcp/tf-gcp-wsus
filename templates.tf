data "template_file" "wsus" {
  template = "${file("${path.module}/include/wsus.tmpl")}"

  vars {
    # Categories
    critical_update    = "${var.wc_critical_update}"
    definition_updates = "${var.wc_definition_updates}"
    feature_packs      = "${var.wc_feature_packs}"
    security_updates   = "${var.wc_security_updates}"
    service_packs      = "${var.wc_service_packs}"
    update_rollups     = "${var.wc_update_rollups}"
    updates            = "${var.wc_updates}"
    drivers            = "${var.wc_drivers}"
    driver_sets        = "${var.wc_driver_sets}"
    tools              = "${var.wc_tools}"
    upgrades           = "${var.wc_upgrades}"

    # Global settings
    language = "${var.wsus_language}"

    # Products
    enabled_products  = "${var.wsus_enabled_products}"
    disabled_products = "${var.wsus_disabled_products}"

    # System config
    timezone = "${var.os_timezone}"
    locale   = "${var.os_locale}"
    envname  = "${var.envname}"

    # Credentials and ADS
    ads_domain_dns       = "${join(",", var.ads_domain_dns)}"
    local_admin_username = "${var.local_admin_username}"
    local_admin_password = "${var.local_admin_password}"
    ads_join_username    = "${var.ads_join_username}"
    ads_join_password    = "${var.ads_join_password}"
    ads_fq_domain_name   = "${var.ads_fq_domain_name}"
    ou                   = "${var.ads_domain_ou}"
  }
}

data "template_file" "wsus_gpo" {
  template = "${file("${path.module}/include/wsus-gpo.tmpl")}"

  vars {
    ads_domain_username           = "${var.ads_join_username}"
    ads_domain_password           = "${var.ads_join_password}"
    ads_fq_domain_name            = "${var.ads_fq_domain_name}"
    gpo_creation                  = "${var.enable_gpo_creation}"
    wsus_client_gpo_name          = "${var.wsus_gpo_name}"
    AcceptTrustedPublisherCerts   = "${var.AcceptTrustedPublisherCerts}"
    DisableWindowsUpdateAccess    = "${var.DisableWindowsUpdateAccess}"
    ElevateNonAdmins              = "${var.ElevateNonAdmins}"
    TargetGroup                   = "${var.TargetGroup}"
    TargetGroupEnabled            = "${var.TargetGroupEnabled}"
    WUServer                      = "${var.WUServer}"
    WUStatusServer                = "${var.WUStatusServer}"
    NoWindowsUpdate               = "${var.NoWindowsUpdate}"
    DisableWindowsUpdateAccess_IC = "${var.DisableWindowsUpdateAccess_IC}"
    DisableWindowsUpdateAccess_WS = "${var.DisableWindowsUpdateAccess_WS}"
    AUOptions                     = "${var.AUOptions}"
    AutoInstallMinorUpdates       = "${var.AutoInstallMinorUpdates}"
    DetectionFrequency            = "${var.DetectionFrequency}"
    DetectionFrequencyEnabled     = "${var.DetectionFrequencyEnabled}"
    NoAutoRebootWithLoggedOnUsers = "${var.NoAutoRebootWithLoggedOnUsers}"
    NoAutoUpdate                  = "${var.NoAutoUpdate}"
    RebootRelaunchTimeout         = "${var.RebootRelaunchTimeout}"
    RebootRelaunchTimeoutEnabled  = "${var.RebootRelaunchTimeoutEnabled}"
    RebootWarningTimeout          = "${var.RebootWarningTimeout}"
    RebootWarningTimeoutEnabled   = "${var.RebootWarningTimeoutEnabled}"
    RescheduleWaitTime            = "${var.RescheduleWaitTime}"
    RescheduleWaitTimeEnabled     = "${var.RescheduleWaitTimeEnabled}"
    ScheduledInstallDay           = "${var.ScheduledInstallDay}"
    ScheduledInstallTime          = "${var.ScheduledInstallTime}"
    UseWUServer                   = "${var.UseWUServer}"
  }
}
