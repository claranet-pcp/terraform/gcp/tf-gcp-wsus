/* Environment */
variable "envname" {
  description = "The environment you're creating this WSUS resource in e.g. 'staging', 'prod'"
  type        = "string"
}

variable "tags" {
  description = "These will be added to the default tags for all resources created by this module"
  type        = "list"
  default     = ["wsus"]
}

/* GCP specific variables */
variable "region" {
  description = "The GCP region to place this WSUS resource in, resources will be distributed automatically"
  type        = "string"
  default     = "europe-west1"
}

variable "gcp_service_account_email" {
  description = "GCP service account to attach to the instance(s)"
  type        = "string"
  default     = ""
}

variable "gcp_service_account_scopes" {
  description = "A list of service scopes for the gcp_service_account_email user, both OAuth2 URLs and gcloud short names are supported"
  type        = "list"

  default = [
    "storage-ro",
  ]
}

variable "startup_script" {
  description = "Add any extra startup scripts you would like to apply to this vm"
  type        = "string"
  default     = ""
}

variable "gcp_machine_type" {
  description = "The size of GCP instance you want to use for this WSUS service"
  type        = "string"
  default     = "n1-standard-2"
}

variable "gcp_network" {
  description = "The name of the network you wish to connect this WSUS service to"
  type        = "string"
  default     = "default"
}

variable "labels" {
  description = "Labels to apply to the VM, must be specified as a map"
  type        = "map"
  default     = {}
}

variable "allow_stopping_for_update" {
  description = "Boolean to allow or dissalow stopping instances for update ie instance size"
  type        = "string"
  default     = true
}

/* Firewall variables */

variable "create_firewall" {
  description = "True or False to create firewall rules for WSUS, choose False if you already have rules to enable this"
  type        = "string"
  default     = true
}

/* Instance specific variables */

variable "vm_name" {
  description = "Name of the virtual machine"
  type        = "string"
}

variable "os_version" {
  description = "The OS version to build the WSUS service with"
  type        = "string"
  default     = "windows-2016"
}

variable "disk_type" {
  description = "The OS disk type for the GCP instances this WSUS service will be built on"
  type        = "string"
  default     = "pd-standard"
}

variable "os_locale" {
  description = "This will be the locale that will be set on the WSUS servers"
  type        = "string"
  default     = "en-GB"
}

variable "os_timezone" {
  description = "This will be the time zone that the WSUS servers will be set to"
  type        = "string"
  default     = "GMT Standard Time"
}

variable "deletion_protection" {
  description = "Protect from API based termination"
  type        = "string"
  default     = false
}

variable "additional_metadata" {
  description = "A map of additional metadata"
  type        = "map"
  default     = {}
}

/* Active directory domain variables */
variable "ads_fq_domain_name" {
  description = "The FQDN of the Active Directory domain these WSUS servers will join"
  type        = "string"
}

variable "ads_join_username" {
  description = "The Active Directory user we'll use to join the WSUS servers to the domain"
  type        = "string"
}

variable "ads_join_password" {
  description = "The password for the Active Directory user we'll use to join the WSUS servers to the domain"
  type        = "string"
}

variable "ads_domain_dns" {
  description = "The list of DNS servers to use in order of princpial, replicas"
  type        = "list"
}

variable "enable_gpo_creation" {
  description = "Declare true or false whether to create the gpo's required to apply WSUS, defaults to false"
  type        = "string"
  default     = false
}

variable "ads_domain_ou" {
  description = "The domain organisational unit you want the WSUS server to be created in"
  type        = "string"
}

/* Local account variables */
variable "local_admin_username" {
  description = "The username you wish to set for the all the default administrator accounts"
  type        = "string"
}

variable "local_admin_password" {
  description = "The password you wish to set for the all the default administrator accounts"
  type        = "string"
}

/* WSUS Variables */
variable "wsus_enabled_products" {
  description = "A comma seperated list of the products you want the WSUS instance to look out for"
  type        = "string"
  default     = "windows server 2008*,windows server 2012*,windows server 2016*"
}

variable "wsus_disabled_products" {
  description = "A comma seperated list of the products you want WSUS to explicitly ignore"
  type        = "string"
  default     = "*language packs*,*drivers*"
}

variable "wsus_drive_size" {
  description = "The size of the data drive (GB) you wish to provision specifically for WSUS"
  type        = "string"
  default     = 200
}

variable "wsus_drive_type" {
  description = "The type of the data drive you wish to provision specifically for WSUS"
  type        = "string"
  default     = "pd-standard"
}

variable "wsus_language" {
  description = "The language you wish to set for WSUS"
  type        = "string"
  default     = "en"
}

variable "wsus_gpo_name" {
  description = "Name of the WSUS GPO"
  type        = "string"
  default     = "WSUS-GPO"
}

/* WSUS Classifications
  At least one of these must be set to 1
*/
variable "wc_critical_update" {
  description = "Whether to switch on this type of update classification"
  type        = "string"
  default     = 1
}

variable "wc_definition_updates" {
  description = "Whether to switch on this type of update classification"
  type        = "string"
  default     = 0
}

variable "wc_feature_packs" {
  description = "Whether to switch on this type of update classification"
  type        = "string"
  default     = 0
}

variable "wc_security_updates" {
  description = "Whether to switch on this type of update classification"
  type        = "string"
  default     = 1
}

variable "wc_service_packs" {
  description = "Whether to switch on this type of update classification"
  type        = "string"
  default     = 0
}

variable "wc_update_rollups" {
  description = "Whether to switch on this type of update classification"
  type        = "string"
  default     = 0
}

variable "wc_updates" {
  description = "Whether to switch on this type of update classification"
  type        = "string"
  default     = 0
}

variable "wc_drivers" {
  description = "Whether to switch on this type of update classification"
  type        = "string"
  default     = 0
}

variable "wc_driver_sets" {
  description = "Whether to switch on this type of update classification"
  type        = "string"
  default     = 0
}

variable "wc_tools" {
  description = "Whether to switch on this type of update classification"
  type        = "string"
  default     = 0
}

variable "wc_upgrades" {
  description = "Whether to switch on this type of update classification"
  type        = "string"
  default     = 0
}
