data "google_compute_zones" "zones" {
  /* Data source for returning up to date information on availability zones */
  status = "UP"
}